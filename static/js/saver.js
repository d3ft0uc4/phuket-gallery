function save() {
    var urls = [];
    var images = document.getElementsByClassName('fullsize-img');
    for (var i = 0; i < images.length; i++) {
        urls.push(images[i].src);
    }
    var nombre = "Images";
    compressed_img(urls, nombre);
}

function compressed_img(urls, nombre) {
    var zip = new JSZip();
    var count = 0;
    var name = nombre + ".zip";
    console.log(urls);
    urls.forEach(function (url) {

        JSZipUtils.getBinaryContent('https://cors-anywhere.herokuapp.com/' + url, function (err, data) {
            if (!err) {
                zip.file(url.split('/').slice(-1)[0], data, {binary: true});
            }
            count++;
            if (count == urls.length) {
                zip.generateAsync({type: 'blob'}).then(function (content) {
                    saveAs(content, name);
                });
            }
        });
    });
}