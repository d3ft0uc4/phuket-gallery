from flask import Flask, render_template
from constants import *
import json
import requests
import mysql.connector

app = Flask(__name__, static_url_path='')


def SELECT(ses, obj):
    response = ses.post(endpoints['SELECT'], data=json.dumps(obj, encoding='windows-1251'), headers=headers)
    return response.text


@app.errorhandler(404)
def not_found(e):
    return render_template('index.html')


@app.route('/favicon.ico')
def favicon():
    return app.send_static_file('favicon.ico')


@app.route('/<site_id>')
def query_photos(site_id):
    try:
        cnx = mysql.connector.connect(**config)
        # session = requests.Session()
        # auth = session.post(url=auth_url, data=json.dumps(credentials), headers=headers)
        images = []
        name = ''
        get_img_fname = ("SELECT filename from realty_image WHERE realty_id=%s")
        name_query = (
            "SELECT model_id,name FROM realty_translation INNER JOIN realty ON realty_translation.model_id = realty.id WHERE language_id = 2 AND model_id=%s")
        cursor = cnx.cursor()
        cursor.execute(get_img_fname, (site_id,))
        for filename in cursor:
            images.append(unicode(filename[0]))
        images = map(lambda item: WWW_STATIC_FOLDER + item, images)
        cursor = cnx.cursor()
        cursor.execute(name_query, (site_id,))
        for _, n in cursor:
            name = n
        context = {'images': images, 'name': name}
        cnx.close()
        app.logger.info('ok')
        return render_template('index.html', **context)
    except Exception as e:
        app.logger.error(e)
        raise e


if __name__ == '__main__':
    app.run(debug=True)
